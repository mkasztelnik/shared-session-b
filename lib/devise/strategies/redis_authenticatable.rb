require 'devise/strategies/base'

module Devise
  module Strategies
    class RedisAuthenticatable < Authenticatable
      def valid?
        session["auth"]
      end

      def authenticate!
        user = User::Checkin.from_omniauth(session["auth"])

        if user.persisted?
          success!(user)
        else
          fail(:invalid_user)
        end
      end
    end
  end
end
